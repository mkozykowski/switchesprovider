package com.mkozykowski.switches;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    SwitchesAdapter myAdapter = new SwitchesAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(myAdapter);
    }

    private class SwitchesAdapter extends BaseAdapter {
        //maksymalna liczba wierszy
        @Override
        public int getCount() {
            return SwitchesProvider.getInstance().getMySwitchStatusSize();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            Holder holder;

            if (convertView == null) {
                //najlepiej zawsze false jako 3 parametr
                convertView = getLayoutInflater().inflate(R.layout.adapter, parent, false);
                holder = new Holder();
                holder.textView = (TextView) convertView.findViewById(R.id.number);
                holder.mySwitch = (Switch) convertView.findViewById(R.id.switch1);
                //final holder = holder
                final Holder newFinalHolder = holder;
                //A button with two states, checked and unchecked. When the button is pressed or clicked, the state changes automatically.
                holder.mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        SwitchesProvider.getInstance().setMySwitchStatus(newFinalHolder.position, isChecked);
                    }
                });
                convertView.setTag(holder);
            } else {
                holder = (Holder) convertView.getTag();
            }

            holder.textView.setText("" + position);
            holder.mySwitch.setChecked(SwitchesProvider.getInstance().getMySwitchStatus(position));
            holder.position = position;
            return convertView;
        }

        private class Holder {
            TextView textView;
            Switch mySwitch;
            int position;
        }
    }
}
