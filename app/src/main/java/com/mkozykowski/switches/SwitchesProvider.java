package com.mkozykowski.switches;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mkozykowski on 19.08.15.
 */
public class SwitchesProvider {

    private static volatile SwitchesProvider switchesProvider;
    private List<Boolean> mySwitchStatus;
    private SwitchesProvider() {
        mySwitchStatus = new ArrayList<>();
        for(int i=0; i<15; i++){
            mySwitchStatus.add(false);
        }
    }

    public static SwitchesProvider getInstance() {
        if (switchesProvider == null ) {
            synchronized (SwitchesProvider.class) {
                if (switchesProvider == null) {
                    switchesProvider = new SwitchesProvider();
                }
            }
        }
        return switchesProvider;
    }

    public Boolean getMySwitchStatus(int position) {
        return mySwitchStatus.get(position);
    }

    public void setMySwitchStatus(int position, Boolean switchStatus) {
        this.mySwitchStatus.set(position,switchStatus);
    }
    public int getMySwitchStatusSize() {
        return this.mySwitchStatus.size();
    }


}
